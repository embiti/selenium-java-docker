FROM selenium/standalone-chrome

LABEL maintainer="Mbiti Elkana <embiti@safaricom.co.ke>"

# ------------------------------------------
# set container environment on local time
# ------------------------------------------
ENV TZ=Africa/Nairobi
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# ------------------------------------------
# Defining proxy variables
# ------------------------------------------
ENV http_proxy "http://172.29.213.98:8080"
ENV https_proxy "https://172.29.213.98:8080"

# ------------------------------------------
# Defining variables
# ------------------------------------------
ARG MAVEN_VERSION="3.6.3"
ENV MAVEN_HOST https://downloads.apache.org
ENV MAVEN_DOWNLOAD_URL ${MAVEN_HOST}/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz
ENV CHROMEDRIVER_VERSION 83.0.4103.39

# ------------------------------------------
# Install Java
# ------------------------------------------
RUN apt-get -y install sudo
RUN sudo apt-get clean
RUN sudo apt-get update
RUN apt-get -y upgrade
RUN sudo apt-get -y upgrade wget
RUN sudo apt-get -y install ca-certificates
RUN sudo update-ca-certificates
RUN apt-get install -y apt-transport-https
RUN sudo apt-get install -y unzip

# ------------------------------------------
# Download maven with wget
# ------------------------------------------
#RUN wget -q http://mirrors.ocf.berkeley.edu/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz -O /apache-maven-${MAVEN_VERSION}-bin.tar.gz
COPY apache-maven-${MAVEN_VERSION}-bin.tar.gz .

# ------------------------------------------
# Unpack maven to container $ROOT
# ------------------------------------------
RUN mkdir -p /opt
RUN tar xvzf /apache-maven-${MAVEN_VERSION}-bin.tar.gz

# ------------------------------------------
# Unpack Java jdk-14
# ------------------------------------------
COPY jdk-14.0.1_linux-x64_bin.tar.gz .
#RUN sudo mkdir /usr/lib/jvm/
#RUN sudo tar -zxvf jdk-14.0.1_linux-x64_bin.tar.gz -C /usr/lib/jvm/
RUN sudo tar -zxvf jdk-14.0.1_linux-x64_bin.tar.gz
#RUN sudo update-alternatives --install /jdk-14.0.1/bin/java java /usr/lib/jvm/jdk-14.0.1/bin/java 1
#RUN sudo update-alternatives --config java

# ------------------------------------------
# Download and install Chromedriver
# ------------------------------------------
#RUN wget -q --continue "http://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip"
RUN wget -N "http://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip" -P ~/Downloads
RUN sudo unzip ~/Downloads/chromedriver_linux64.zip -d ~/Downloads
RUN sudo mv -f ~/Downloads/chromedriver /usr/bin
RUN sudo chmod +x /usr/bin/chromedriver
#RUN sudo mv -f ~/Downloads/chromedriver /usr/local/share/chromedriver
#Change the directory to /usr/bin/chromedriver
#RUN sudo ln -s /usr/local/share/chromedriver /usr/local/bin/chromedriver
#RUN sudo ln -sf /usr/local/share/chromedriver /usr/bin/chromedriver

#COPY chromedriver_linux64.zip .
#RUN sudo unzip chromedriver_linux64.zip

RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN sudo dpkg -i google-chrome-stable_current_amd64.deb

# ------------------------------------------
# Further environment variables
# ------------------------------------------
ENV M2_HOME="apache-maven-${MAVEN_VERSION}"
ENV MAVEN_HOME="apache-maven-${MAVEN_VERSION}"
ENV PATH "$PATH:/apache-maven-${MAVEN_VERSION}/bin"
ENV PATH="$PATH:/jdk-14.0.1/bin"
ENV JAVA_HOME="/jdk-14.0.1/"

# ------------------------------------------
# Debugging other linux commands
# ------------------------------------------
RUN sudo apt-get -f install
RUN sudo dpkg --configure -a
RUN google-chrome --version
RUN whereis google-chrome
RUN whereis google-chrome-stable
RUN /usr/bin/google-chrome-stable --version
RUN sudo update-alternatives --config google-chrome
RUN ls -laog /usr/bin/google-chrome
RUN ls -laog /etc/alternatives/google-chrome
RUN ls -laog /opt/google/chrome/google-chrome
RUN sudo update-alternatives --config google-chrome
RUN ls -la /usr/bin/

# ------------------------------------------
# define working directory
# ------------------------------------------
WORKDIR /playground

# ------------------------------------------
# copy required files from local workspace
# to working directory in Docker container
# ------------------------------------------
COPY . /playground

# ------------------------------------------
# list files in working directory
# ------------------------------------------
RUN ls -la ./

# ------------------------------------------
# define entry point. When you pass commands
# to container what should interpret them
# ------------------------------------------
RUN java -version
RUN mvn -version
RUN echo "$PWD"
# ------------------------------------------
# list files in working directory
# ------------------------------------------
RUN ls -la ./
ENTRYPOINT ["bash", "catalina.sh"]
# ------------------------------------------
# list files in working directory
# ------------------------------------------
RUN ls -la ./