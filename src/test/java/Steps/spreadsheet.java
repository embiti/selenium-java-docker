package Steps;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
//import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//import com.google.common.collect.Table.Cell;

public class spreadsheet {

	public static String URL;
	public static String username;
	public static String password;
	public static int MSISDN;

    public void readExcel(String filePath,String fileName,String sheetName) throws IOException{

    //Create an object of File class to open xlsx file

    File file =    new File(filePath+"//"+fileName);

    //Create an object of FileInputStream class to read excel file

    FileInputStream inputStream = new FileInputStream(file);
    
    Workbook testdata = null;
    
    //Find the file extension by splitting file name in substring  and getting only extension name

    String fileExtensionName = fileName.substring(fileName.indexOf("."));

    //Check condition if the file is xlsx file

    if(fileExtensionName.equals(".xlsx")){

    //If it is xlsx file then create object of XSSFWorkbook class

    testdata = new XSSFWorkbook(inputStream);

    }

    //Check condition if the file is xls file

    else if(fileExtensionName.equals(".xls")){

        //If it is xls file then create object of HSSFWorkbook class

    testdata = new HSSFWorkbook(inputStream);

    }

    //Read sheet inside the workbook by its name

    Sheet testdataSheet = testdata.getSheet(sheetName);

    //Find number of rows in excel file

    int rowCount = testdataSheet.getLastRowNum()-testdataSheet.getFirstRowNum();
    System.out.println(rowCount);

    //Create a loop over all the rows of excel file to read it

    for (int i = 0; i < rowCount+1; i++) {

        Row row = testdataSheet.getRow(i);

        //Create a loop to print cell values in a row

        for (int j = 0; j < row.getLastCellNum(); j++) {

            //Print Excel data in console
        	
        	//GET CELL
            Cell cell = row.getCell(0); 
            Cell cell1 = row.getCell(1);
            Cell cell2 = row.getCell(2);
            Cell cell3 = row.getCell(3);
            
            //Cell cell2 = testdataSheet.getCell(j, i);
            //data2[j][i] = cell.getContents();

            //String Username = testdataSheet.getCell(0).getContents();
            //System.out.println("Username" +Username);
            
            //SET AS STRING TYPE
            //cell.setCellType(Cell.STRING);
            //cell.setCellValue(STRING);
            //String data= cell.getStringCellValue();
            //System.out.print(data+" | ");
            
            //System.out.print(row.getCell(j).getStringCellValue()+" | ");
            
            //String data;
			//public static String data;
            /*if(cell.getCellType()==CellType.STRING) {
            	spreadsheet.username = cell.getStringCellValue();
            	//System.out.print(data+" | ");
            }else if(cell.getCellType()==CellType.NUMERIC) {
            	spreadsheet.username = String.valueOf(cell.getNumericCellValue());
            	//System.out.print(data+" | ");
            }else System.out.println("Unable to run code");*/

            
            if(cell.getCellType()==CellType.NUMERIC) {
            	spreadsheet.URL = String.valueOf(cell.getNumericCellValue());
            	//System.out.print(data+" | ");
            }else spreadsheet.URL = cell.getStringCellValue();
            
            if(cell1.getCellType()==CellType.NUMERIC) {
            	spreadsheet.username = String.valueOf(cell1.getNumericCellValue());
            	//System.out.print(data+" | ");
            }else spreadsheet.username = cell1.getStringCellValue();
            
            if(cell2.getCellType()==CellType.NUMERIC) {
            	spreadsheet.password = String.valueOf(cell2.getNumericCellValue());
            	//System.out.print(data+" | ");
            }else spreadsheet.password = cell2.getStringCellValue();
            
            /*if(cell3.getCellType()==CellType.NUMERIC) {
            	spreadsheet.MSISDN = String.valueOf(cell3.getNumericCellValue());
            	//System.out.print(data+" | ");
            }else spreadsheet.MSISDN = cell3.getStringCellValue();*/
            
            spreadsheet.MSISDN= (int)cell3.getNumericCellValue();
            
            //int data2= (int)cell3.getNumericCellValue();

        }
        
        //System.out.println();
    }

 }
    

    //Main function is calling readExcel function to read data from excel file
    //public static void main(String[] args) throws IOException {
    //public static void readExcel(String methodName) throws IOException {
    public static void readExcel(String...strings) throws IOException{
    
    spreadsheet objExcelFile = new spreadsheet();

    //Path of excel file
    String filePath = System.getProperty("user.dir")+"//src//testdata";

    //Call read excel method of the class to read data
    objExcelFile.readExcel(filePath,"testdata.xlsx","Sheet1");
    /*objExcelFile.readExcel(filePath,"testdata.xlsx","Sheet2");
    objExcelFile.readExcel(filePath,"testdata.xlsx","Sheet3");
    objExcelFile.readExcel(filePath,"testdata.xlsx","Sheet4");
    objExcelFile.readExcel(filePath,"testdata.xlsx","Sheet5");*/

    }

}