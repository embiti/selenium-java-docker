package Runtest;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "./src/test/java/Features/Enter.feature", glue = {
		"Steps" }, plugin = { "pretty", "json:target/cucumber.json",
				"html:target/testResult/cucumber-html-report",
				"junit:target/testResult/cucumber-xml-report/cucumber.xml", "rerun:target/testResult/rerun.txt" }
)

public class Runner {

}

//mvn clean install -DproxySet=true -DproxyHost=http://proxy2 -DproxyPort=8080
//mvn clean install -DproxySet=true -DproxyHost=http://proxy2 -DproxyPort=8080
//mvn clean verify
//git status && git add . && git commit -m "Initial commit" && git push -u origin master
//mvn clean install -DproxySet=true -DproxyHost=proxy2 -DproxyPort=8080
//docker run -dit --name crmApache3 -p 1993:80 -v /data/docker/volumes/cucumberOutput/_data/target/testResult/cucumber-maven-report/cucumber-html-reports/:/usr/local/apache2/htdocs/ httpd:2.4